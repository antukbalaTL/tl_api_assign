const http = require('http');
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const dotenv = require('dotenv');
var path = require('path');
var fs = require('fs');
const morgan = require('morgan');
require('dotenv').config();


const ownerRoute = require('./api/routes/ownerRoute');
const shipperRoute = require('./api/routes/shipperRoute');
const tripRoute = require('./api/routes/tripRoute');
const userRoute = require('./api/routes/userRoute');


var accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs/access.log'), { flags: 'a' })

app.use(express.json());
app.use(morgan('combined', { stream: accessLogStream }))
app.use(morgan('dev'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));


app.use('/owners', ownerRoute);
app.use('/shippers', shipperRoute);
app.use('/trips', tripRoute);
app.use('/users', userRoute);


const port = 8100;
const server = http.createServer(app);
server.listen(port, (req, res, next) => {
    console.log(`server running on http://localhost:${port}/`);
});
