const express = require('express');
const router = express.Router();
const shippperController = require('../controllers/shipperController');
const shippperController2 = require('../controllers/shipperController2');


// get all shipper
// router.get('/', shippperController.getAllShipper);
router.get('/', [
    shippperController2.getAllShipper
]);

// get shipper by id
// router.get('/:shipperId', shippperController.getShipperById);
router.get('/:shipperId', [
    shippperController2.getShipperById
]);

router.get('/:shipperId/trips', [
    shippperController.getAllTripByIdPagination
]);


module.exports = router;
