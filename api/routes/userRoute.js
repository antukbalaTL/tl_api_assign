const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const userController2 = require('../controllers/userController2');
const ValidationMiddleware = require('../middleware/authValidation');


// router.get('/', userController.getAllUser);
router.get('/', [
    userController2.getAllUser
]);


router.post('/login', [
    userController2.login
]);


// router.get('/:userId', userController.getUserById);
router.get('/:userId', [
    userController2.getUserById
]);

// router.post('/add', userController.addNewUser);
router.post('/add', [
    userController2.addNewUser
]);


// router.delete('/:userId', userController.deleteUserById);
router.delete('/:userId', [
    ValidationMiddleware.validJWTNeeded,
    userController2.deleteUserById
]);


module.exports = router;
