const express = require('express');
const router = express.Router();
const tripController = require('../controllers/tripController');
const tripController2 = require('../controllers/tripController2');


// router.get('/', tripController.getAllTrips);
router.get('/', [
    tripController2.getAllTrips
]);


// router.get('/:requestId', tripController.getTripInfoById);
router.get('/:requestId', [
    tripController2.getTripInfoById
]);


// router.get('/:requestId/bids', tripController.getBidCountById);
router.get('/:requestId/bids', [
    tripController2.getBidCountById
]);


module.exports = router;
