const express = require('express');
const router = express.Router();
const ownerController = require('../controllers/ownerController');
const ownerController2 = require('../controllers/ownerController2');
const ValidationMiddleware = require('../middleware/authValidation');


// get all owner
// router.get('/', ownerController.getAllOwner);
router.get('/', [
    ownerController2.getAllOwner
]);


router.get('/test', [
    ValidationMiddleware.generateSecretToken
]);


// get owner by id
// router.get('/:ownerId', ownerController.getOwnerById);
router.get('/:ownerId', [
    ownerController2.getOwnerById
]);


// delete owner by id
// router.delete('/:ownerId', ownerController.deleteOwnerById);
router.delete('/:ownerId', [
    ownerController2.deleteOwnerById
]);


module.exports = router;
