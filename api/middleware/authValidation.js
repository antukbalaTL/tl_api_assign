const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const res = require('express/lib/response');


const generateSecretToken = (req, res) => {
    const key = crypto.randomBytes(64).toString('hex');
    res.json(key);
    console.log(key);
};


const generatateAccessToken = (fullName, userId) => {
    return jwt.sign(
        {
            fullName: fullName,
            userId: userId
        },
        process.env.TOKEN_SECRET,
        {
            expiresIn: '20m'
        }
    );
};


const validJWTNeeded = (req, res, next) => {
    if (req.headers['authorization']) {
        try {
            let authorization = req.headers['authorization'].split(' ');

            if (authorization[0] !== 'Bearer') {
                return res.json('Token not found');
            }
            else {
                req.jwt = jwt.verify(authorization[1], process.env.TOKEN_SECRET);
                return next();
            }
        } catch (error) {
            if (error.message == 'jwt expired') {
                return res.json('Token Expired');
            }
            else {
                return res.json('Token Authorization Failed');
            }
        }
    }
    else {
        return res.json('Token not found')
    }
};


module.exports = {
    generateSecretToken,
    generatateAccessToken,
    validJWTNeeded
};
