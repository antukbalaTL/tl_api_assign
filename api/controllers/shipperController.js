const Shipper = require('../models/Shipper');


const getAllShipper = async (req, res) => {
    let finalOutput = {};

    try {
        const shippers = await Shipper.getAllShipper();
        const totalShippers = shippers.length;

        finalOutput.routeDescription = 'gel all shipper';
        finalOutput.totalShippers = totalShippers;
        finalOutput.shippers = shippers;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
}


const getShipperById = async (req, res) => {
    let finalOutput = {};

    try {
        const shipperId = req.params.shipperId;

        const shipperDetails = await Shipper.getShipperById(shipperId);
        finalOutput.routeDescription = 'get shipper by id';

        if (shipperDetails.length == 0) {
            finalOutput.notFound = 'No Shipper found for this ID'
        }
        else {
            finalOutput.shipperDetails = shipperDetails;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
}


const getAllTripByIdPagination = async (req, res) => {
    let finalOutput = {};

    try {
        const shipperId = req.params.shipperId;

        trips = await Shipper.getAllTripByIdPagination(shipperId);
        totalTrips = trips.length;

        finalOutput.routeDescription = 'get all trip of a shipper by ID, used pagination';
        finalOutput.totalTrips = totalTrips;
        finalOutput.trips = trips;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    };
}

module.exports = {
    getAllShipper,
    getShipperById,
    getAllTripByIdPagination
}
