const User2 = require('../models/User2');
const ValidationMiddleware = require('../middleware/authValidation');

const getAllUser = async (req, res) => {
    let finalOutput = {};

    try {
        const allUser = await User2.getAllUser();

        finalOutput.routeDescription = 'get all user';
        finalOutput.totalUser = allUser.length;
        finalOutput.allUser = allUser;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getUserById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.userId;
        const user = await User2.getUserById(userId);

        finalOutput.routeDescription = 'get user by ID';

        if (user.length == 0) {
            finalOutput.notFound = 'No User found for this ID';
        }
        else {
            finalOutput.user = user;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const addNewUser = async (req, res) => {
    let finalOutput = {};

    const userId = req.body.userId;
    const userType = req.body.userType;
    const userTypeCategory = req.body.userTypeCategory;
    const fullName = req.body.fullName;
    const companyName = req.body.companyName;

    try {
        const newUser = await User2.addUser(userId, userType, userTypeCategory, fullName, companyName);
        // const addingUser = await newUser.addUser();

        finalOutput.routeDescription = 'Add new User';
        finalOutput.newUser = {
            userId: userId,
            userType: userType,
            userTypeCategory: userTypeCategory,
            fullName: fullName,
            companyName: companyName
        };

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const deleteUserById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.userId;
        const deletedUser = await User2.getUserById(userId);

        finalOutput.routeDescription = 'delete user by ID';

        if (deletedUser.length == 0) {
            finalOutput.notFound = 'No User found for this ID';
        }
        else {
            const deleted = await User2.deleteUserById(userId);
            finalOutput.deletedUser = deletedUser;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const login = async (req, res) => {
    const { fullName, userId } = req.body;
    let finalOutput = {};

    try {
        const dbUserId = await User2.getUserIdbyName(fullName);

        // console.log(dbUserId, typeof dbUserId, typeof userId);
        finalOutput.routeDescription = 'user login and jwt';

        if (userId == dbUserId) {
            const token = ValidationMiddleware.generatateAccessToken(fullName, userId);

            finalOutput.fullName = fullName;
            finalOutput.userId = userId;
            finalOutput.token = token;
        }
        else {
            finalOutput.authError = "username and password doesn't match";
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


module.exports = {
    getAllUser,
    getUserById,
    addNewUser,
    deleteUserById,
    login
};
