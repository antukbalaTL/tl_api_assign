const Trip = require('../models/Trip');


const getAllTrips = async (req, res) => {
    let finalOutput = {};

    try {
        const allTrips = await Trip.getAllTrips();
        const totalTrips = allTrips.length;

        finalOutput.routeDescription = 'get all trips';
        finalOutput.totalTrips = totalTrips;
        finalOutput.allTrips = allTrips;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getTripInfoById = async (req, res) => {
    let finalOutput = {};

    try {
        const requestId = req.params.requestId;

        const tripDetails = await Trip.getTripInfoById(requestId);

        finalOutput.routeDescription = 'get trip info by ID';

        if (tripDetails) {
            finalOutput.tripDetails = tripDetails[0];
        }
        else {
            finalOutput.notFound = 'No Trip found for this Request ID'
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getBidCountById = async (req, res) => {
    let finalOutput = {};

    try {
        const requestId = req.params.requestId;
        const totalBids = await Trip.getBidCountById(requestId);

        finalOutput.routeDescription = 'get bid count of a Trip by request ID';
        finalOutput.totalBids = totalBids;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


module.exports = {
    getAllTrips,
    getTripInfoById,
    getBidCountById
}
