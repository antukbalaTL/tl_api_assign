const User = require('../models/User');


const getAllUser = async (req, res) => {
    let finalOutput = {};

    try {
        const allUser = await User.getAllUser();

        finalOutput.routeDescription = 'get all user';
        finalOutput.totalUser = allUser.length;
        finalOutput.allUser = allUser;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getUserById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.userId;
        const user = await User.getUserById(userId);

        finalOutput.routeDescription = 'get user by ID';

        if (user.length == 0) {
            finalOutput.notFound = 'No User found for this ID';
        }
        else {
            finalOutput.user = user;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const addNewUser = async (req, res) => {
    let finalOutput = {};

    const userId = req.body.userId;
    const userType = req.body.userType;
    const userTypeCategory = req.body.userTypeCategory;
    const fullName = req.body.fullName;
    const companyName = req.body.companyName;

    try {
        const newUser = new User(userId, userType, userTypeCategory, fullName, companyName);
        const addingUser = await newUser.addUser();

        finalOutput.routeDescription = 'Add new User';
        finalOutput.newUser = newUser;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const deleteUserById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.userId;
        const deletedUser = await User.getUserById(userId);

        finalOutput.routeDescription = 'delete user by ID';

        if (deletedUser.length == 0) {
            finalOutput.notFound = 'No User found for this ID';
        }
        else {
            const deleted = await User.deleteUserById(userId);
            finalOutput.deletedUser = deletedUser;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


module.exports = {
    getAllUser,
    getUserById,
    addNewUser,
    deleteUserById
}
