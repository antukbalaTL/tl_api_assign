const Owner2 = require('../models/Owner2');
const User = require('../models/User');


const getAllOwner = async (req, res) => {
    let finalOutput = {};

    try {
        const owners = await Owner2.getAllOwner();
        const totalOwners = owners.length;

        finalOutput.routeDescription = 'get all owner';
        finalOutput.totalOwners = totalOwners;
        finalOutput.owners = owners;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getOwnerById = async (req, res) => {
    let finalOutput = {};

    try {
        const ownerId = req.params.ownerId;

        const ownerDetails = await Owner2.getOwnerById(ownerId);
        finalOutput.routeDescription = 'get owner by id';

        if (ownerDetails.length == 0) {
            finalOutput.notFound = 'No Owner found for this ID'
        }
        else {
            finalOutput.ownerDetails = ownerDetails;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const deleteOwnerById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.ownerId;

        const deletedUser = await User.getUserById(userId);
        finalOutput.routeDescription = "delete owner by id";

        if (deletedUser.length == 0) {
            finalOutput.notFound = 'No Owner found for this ID';
        }
        else {
            const deleted = await Owner2.deleteOwnerById(userId);

            finalOutput.deletedUser = deletedUser;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
}


module.exports = {
    getAllOwner,
    getOwnerById,
    deleteOwnerById
};
