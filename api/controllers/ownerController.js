const Owner = require('../models/Owner');
const User = require('../models/User');

// const getAllOwner2 = async (req, res) => {
//     console.log('yeah!!');

//     let finalOutput = {};

//     try {
//         const owners = await Owner.getAllOwner2();
//         const totalOwners = owners.length;

//         finalOutput.routeDescription = 'get all owner';
//         finalOutput.totalOwners = totalOwners;
//         finalOutput.owners = owners;

//         res.json(finalOutput);
//     } catch (error) {
//         finalOutput.error = 'Some error occured';
//         console.log(error);
//         res.json(finalOutput);
//     }
// };


const getAllOwner = async (req, res) => {
    let finalOutput = {};

    try {
        const owners = await Owner.getAllOwner();
        const totalOwners = owners.length;

        finalOutput.routeDescription = 'get all owner';
        finalOutput.totalOwners = totalOwners;
        finalOutput.owners = owners;

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


const getOwnerById = async (req, res) => {
    let finalOutput = {};

    try {
        const ownerId = req.params.ownerId;

        const ownerDetails = await Owner.getOwnerById(ownerId);
        finalOutput.routeDescription = 'get owner by id';

        if (ownerDetails.length == 0) {
            finalOutput.notFound = 'No Owner found for this ID'
        }
        else {
            finalOutput.ownerDetails = ownerDetails;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
};


// const getOwnerById2 = async (req, res) => {
//     console.log('yeah!!');

//     let finalOutput = {};

//     try {
//         const ownerId = req.params.ownerId;

//         const ownerDetails = await Owner.getOwnerById2(ownerId);
//         finalOutput.routeDescription = 'get owner by id';

//         if (ownerDetails.length == 0) {
//             finalOutput.notFound = 'No Owner found for this ID';
//         }
//         else {
//             finalOutput.ownerDetails = ownerDetails;
//         }

//         res.json(finalOutput);
//     } catch (error) {
//         finalOutput.error = 'Some error occured';
//         console.log(error);
//         res.json(finalOutput);
//     }
// };


const deleteOwnerById = async (req, res) => {
    let finalOutput = {};

    try {
        const userId = req.params.ownerId;

        const deletedUser = await User.getUserById(userId);
        finalOutput.routeDescription = "delete owner by id";

        if (deletedUser.length == 0) {
            finalOutput.notFound = 'No Owner found for this ID';
        }
        else {
            const deleted = await Owner.deleteOwnerById(userId);

            finalOutput.deletedUser = deletedUser;
        }

        res.json(finalOutput);
    } catch (error) {
        finalOutput.error = 'Some error occured';
        console.log(error);
        res.json(finalOutput);
    }
}


module.exports = {
    getAllOwner,
    // getAllOwner2,
    getOwnerById,
    // getOwnerById2,
    deleteOwnerById
}
