require('dotenv').config();
const mysql = require('mysql2');


/* create mysql db connection */
const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD
});


// pool.query(
//     'SELECT * FROM trucklagbe_db.bid_status_details WHERE bid_status_details.status = ? limit 5',
//     ['active'],
//     function (err, results) {
//         // console.log(results);
//         return results;
//     }
// );

module.exports = pool.promise();
