const connTLDB = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const sqlQuery = require('./sqlQueryString');


const getAllShipper = async () => {
    const con = await connTLDB.dbConnection();

    try {
        const shipper = await connTLDB.doQuery(con, sqlQuery.shipper.getAllShipper, []);
        return shipper;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getShipperById = async (shipperId) => {
    const con = await connTLDB.dbConnection();

    try {
        const shipper = await connTLDB.doQuery(con, sqlQuery.shipper.getShipperById, [shipperId]);
        return shipper[0];
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


// not completed for using
const getAllTripByIdPagination = async (shipperId) => {
    try {
        var con = await db.dbConnection();
        const totalTrips = await db.doQuery(con, sqlQuery.shipper.totlaTripsCount, [shipperId]);
        this.skips += 10;

        console.log(totalTrips);

        if (this.skips > totalTrips[0].value) {
            this.skips = 0;
        }

        const trips = await db.doQuery(con, sqlQuery.shipper.getAllTripByIdPagination, [shipperId, this.skips]);
        con.release();
        return trips[0];

    } catch (error) {
        console.log(error);
        res.json({ err: error });
    }
}


module.exports = {
    getAllShipper,
    getShipperById,
    getAllTripByIdPagination
};
