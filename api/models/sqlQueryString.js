const owner = {
    'getAllOwner': `SELECT * FROM trucklagbe_db.user_details where user_type = 'owner' limit 700;`,
    'getOwnerById': `SELECT * FROM trucklagbe_db.user_details where user_type = 'owner' and user_id=?;`,
    'deleteOwnerById': `DELETE FROM trucklagbe_db.user_details WHERE user_id = ?;`
};


const shipper = {
    'totlaTripsCount': `SELECT count(request_id) as value FROM trucklagbe_db.trip_request_details WHERE shipper_id = ?;`,
    'getAllShipper': `SELECT * FROM trucklagbe_db.user_details where user_type = 'shipper' limit 700;`,
    'getShipperById': `SELECT * FROM trucklagbe_db.user_details where user_type = 'shipper' and user_id=?;`,
    'getAllTripById': `SELECT * FROM trucklagbe_db.trip_request_details where shipper_id = ?;`,
    'getAllTripByIdPagination': `SELECT * FROM trucklagbe_db.trip_request_details WHERE shipper_id = ? ORDER BY request_id DESC LIMIT 10 OFFSET ?;`
};


const trip = {
    'getAllTrips': `SELECT * FROM trucklagbe_db.trip_request_details;`,
    'getTripInfoByIdT1': `SELECT * FROM trucklagbe_db.trip_request_details where request_id=?;`,
    'getTripInfoByIdT2': `SELECT * FROM trucklagbe_db.trip_request_details_back_up where request_id=?;`,
    'getTripInfoByIdT3': `SELECT * FROM trucklagbe_db.trip_request_details_back_up_archived where request_id=?;`,
    'getBidCountById': `SELECT count(*) as count FROM (SELECT * FROM trucklagbe_db.bidding_request_bid_details WHERE request_id = ? UNION SELECT * FROM trucklagbe_db.bidding_request_bid_details_back_up WHERE request_id = ? UNION SELECT * FROM trucklagbe_db.bidding_request_bid_details_back_up_archived WHERE request_id = ?) m;`
};


const user = {
    'getAllUser': `SELECT * FROM trucklagbe_db.user_details limit 500;`,
    'getUserById': `SELECT * FROM trucklagbe_db.user_details WHERE user_id = ?;`,
    'addUser': `insert into trucklagbe_db.user_details (user_id, user_type, user_type_category, full_name, company_name) values (?, ?, ?, ?, ?);`,
    'deleteUserById': `DELETE FROM trucklagbe_db.user_details WHERE user_id = ?;`,
    'getUserIdbyName': `SELECT user_id as userId FROM trucklagbe_db.user_details WHERE full_name = ?;`
};


module.exports = {
    owner,
    shipper,
    trip,
    user
}
