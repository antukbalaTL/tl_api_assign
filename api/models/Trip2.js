const connTLDB = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const sqlQuery = require('./sqlQueryString');


const getAllTrips = async () => {
    const con = await connTLDB.dbConnection();

    try {
        const allTrips = await connTLDB.doQuery(con, sqlQuery.trip.getAllTrips, []);
        return allTrips;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getTripInfoById = async (requestId) => {
    const con = await connTLDB.dbConnection();

    try {
        let tripDetails = await connTLDB.doQuery(con, sqlQuery.trip.getTripInfoByIdT1, [requestId]);

        if (tripDetails.length == 0) {
            tripDetails = await connTLDB.doQuery(con, sqlQuery.trip.getTripInfoByIdT2, [requestId]);

            if (tripDetails.length == 0) {
                tripDetails = await connTLDB.doQuery(con, sqlQuery.trip.getTripInfoByIdT3, [requestId]);

                if (tripDetails.length == 0) {
                    return 0;
                }
                else {
                    return tripDetails;
                }
            }
            else {
                return tripDetails;
            }
        }
        else {
            return tripDetails;
        }
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getBidCountById = async (requestId) => {
    const con = await connTLDB.dbConnection();

    try {
        const result = await connTLDB.doQuery(con, sqlQuery.trip.getBidCountById, [requestId, requestId, requestId]);
        return result[0].count;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


module.exports = {
    getAllTrips,
    getTripInfoById,
    getBidCountById
};
