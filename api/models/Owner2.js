const connTLDB = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const sqlQuery = require('./sqlQueryString');

const getAllOwner = async () => {
    let con = await connTLDB.dbConnection();

    try {
        const owner = await connTLDB.doQuery(con, sqlQuery.owner.getAllOwner, []);
        return owner[0];
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getOwnerById = async (ownerId) => {
    let con = await connTLDB.dbConnection();

    try {
        const owner = await connTLDB.doQuery(con, sqlQuery.owner.getOwnerById, [ownerId]);
        return owner[0];
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const deleteOwnerById = async (ownerId) => {
    const con = await connTLDB.dbConnection();

    try {
        const owner = await connTLDB.doQuery(con, sqlQuery.owner.deleteOwnerById, [ownerId]);
        return owner[0];
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


module.exports = {
    getAllOwner,
    getOwnerById,
    deleteOwnerById
};
