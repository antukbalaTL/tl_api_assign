const connectionTrucklagbeDB = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const db = require('../db/db');
const sqlQuery = require('./sqlQueryString');


class Owner {
    constructor(userId, userTypeCategory, createDate) {
        this.userId = userId;
        this.userTypeCategory = userTypeCategory;
        this.createDate = createDate;
    }

    static async getAllOwner() {
        try {
            const owner = await db.execute(sqlQuery.owner.getAllOwner);
            return owner[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }

    static async getOwnerById(ownerId) {
        try {
            const owner = await db.execute(sqlQuery.owner.getOwnerById, [ownerId]);
            return owner[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }

    static async deleteOwnerById(ownerId) {
        try {
            const owner = await db.execute(sqlQuery.owner.deleteOwnerById, [ownerId]);
            return owner[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }
}

module.exports = {
    Owner
};
