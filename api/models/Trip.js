const db = require('../db/db');
const sqlQuery = require('./sqlQueryString');


class Trip {
    constructor(requestId) {
        this.requestId = requestId;
    }


    static async getAllTrips() {
        try {
            const allTrips = await db.execute(sqlQuery.trip.getAllTrips);
            return allTrips[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    static async getTripInfoById(requestId) {
        try {
            let tripDetails = await db.execute(sqlQuery.trip.getTripInfoByIdT1, [requestId]);

            if (tripDetails[0].length == 0) {
                tripDetails = await db.execute(sqlQuery.trip.getTripInfoByIdT2, [requestId]);

                if (tripDetails[0].length == 0) {
                    tripDetails = await db.execute(sqlQuery.trip.getTripInfoByIdT3, [requestId]);

                    if (tripDetails[0].length == 0) {
                        return 0;
                    }
                    else {
                        return tripDetails[0];
                    }
                }
                else {
                    return tripDetails[0];
                }
            }
            else {
                return tripDetails[0];
            }
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    static async getBidCountById(requestId) {
        try {
            const result = await db.execute(sqlQuery.trip.getBidCountById, [requestId, requestId, requestId]);
            return result[0][0].count;
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }
}


module.exports = Trip;
