const db = require('../db/db');
const sqlQuery = require('./sqlQueryString');


class User {
    constructor(userId, userType, userTypeCategory, fullName, companyName) {
        this.userId = userId;
        this.userType = userType;
        this.userTypeCategory = userTypeCategory;
        this.fullName = fullName;
        this.companyName = companyName;
    }


    static async getAllUser() {
        try {
            const allUser = await db.execute(sqlQuery.user.getAllUser);
            return allUser[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    static async getUserById(userId) {
        try {
            const user = await db.execute(sqlQuery.user.getUserById, [userId]);
            return user[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    async addUser() {
        try {
            const newUser = await db.execute(sqlQuery.user.addUser, [this.userId, this.userType, this.userTypeCategory, this.fullName, this.companyName]);
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    static async deleteUserById(userId) {
        try {
            const user = await db.execute(sqlQuery.user.deleteUserById, [userId]);
            return user[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }
}


module.exports = User;
