const db = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const sqlQuery = require('./sqlQueryString');

class Shipper {
    constructor(userId, userTypeCategory, createDate) {
        this.userId = userId;
        this.userTypeCategory = userTypeCategory;
        this.createDate = createDate;
    }

    static skips = -10;

    static async getAllShipper() {
        try {
            const shipper = await db.execute(sqlQuery.shipper.getAllShipper);
            return shipper[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }

    static async getShipperById(shipperId) {
        try {
            const shipper = await db.execute(sqlQuery.shipper.getShipperById, [shipperId]);
            return shipper[0];
        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }


    static async getAllTripByIdPagination(shipperId) {
        try {
            var con = await db.dbConnection();
            const totalTrips = await db.doQuery(con, sqlQuery.shipper.totlaTripsCount, [shipperId]);
            this.skips += 10;

            if (this.skips > totalTrips[0].value) {
                this.skips = 0;
            }

            const trips = await db.doQuery(con, sqlQuery.shipper.getAllTripByIdPagination, [shipperId, this.skips]);
            con.release();
            return trips;

        } catch (error) {
            console.log(error);
            res.json({ err: error });
        }
    }
}

module.exports = Shipper;
