const connTLDB = require('../../libs/dbConnect/mysql/trucklagbeDbConn');
const sqlQuery = require('./sqlQueryString');

const getAllUser = async () => {
    const con = await connTLDB.dbConnection();

    try {
        const allUser = await connTLDB.doQuery(con, sqlQuery.user.getAllUser, []);
        return allUser;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getUserById = async (userId) => {
    const con = await connTLDB.dbConnection();

    try {
        const user = await connTLDB.doQuery(con, sqlQuery.user.getUserById, [userId]);
        return user;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const addUser = async (userId, userType, userTypeCategory, fullName, companyName) => {
    const con = await connTLDB.dbConnection();

    try {
        const newUser = await connTLDB.doQuery(con, sqlQuery.user.addUser, [userId, userType, userTypeCategory, fullName, companyName]);
        // const newUser = await db.execute(sqlQuery.user.addUser, [this.userId, this.userType, this.userTypeCategory, this.fullName, this.companyName]);
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const deleteUserById = async (userId) => {
    const con = await connTLDB.dbConnection();

    try {
        const user = await connTLDB.doQuery(con, sqlQuery.user.deleteUserById, [userId]);
        return user[0];
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


const getUserIdbyName = async (fullName) => {
    const con = await connTLDB.dbConnection();

    try {
        const userId = await connTLDB.doQuery(con, sqlQuery.user.getUserIdbyName, [fullName]);

        return userId[0].userId;
    } catch (error) {
        throw error;
    } finally {
        con.release();
    }
};


module.exports = {
    getAllUser,
    getUserById,
    addUser,
    deleteUserById,
    getUserIdbyName
};
