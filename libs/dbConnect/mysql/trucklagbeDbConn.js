'use strict';

var mysql = require('mysql');


const DBOptionsForTLdb = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    dialect: process.env.DB_DIALECT,
    connectionLimit: 100,

    pool: {
        min: 10,
        max: 100
    }
};


var trucklagbeDbConn = mysql.createPool({
    host: DBOptionsForTLdb.host,
    user: DBOptionsForTLdb.user,
    password: DBOptionsForTLdb.password,
    database: DBOptionsForTLdb.database,
    // dialect: DBOptionsForTLdb.dialect,
    // timezone: 'Asia/Dhaka',
    // define: { timestamps: false, charset: "utf8", dialectOptions: { collate: "utf8_general_ci" } },
    connectionLimit: 100,
    pool: {
        max: 100,
        min: 10,
        acquire: 30000
    }
});


const dbConnection = () => {
    return new Promise((resolve, reject) => {
        trucklagbeDbConn.getConnection(function (err, con) {
            if (err) {
                reject(err.sqlMessage);
                return;
            }

            resolve(con);
        });
    });
};


const doQuery = (con, sql, args) => {
    return new Promise(function (resolve, reject) {
        const executedQuery = con.query(sql, args, function (err, result) {
            if (err) return reject(err);

            resolve(result);
        });
    });
};


const isConnectionEstablished = async () => {
    try {
        const conn = await dbConnection();

        try {
            console.log('successfully connected to trucklagbe_db');
        } finally {
            console.log('disconnected from trucklagbe_db');
            conn.release();
        }
    } catch (err) {
        console.log('unsuccessful connection to trucklagbe_db', err);
    }
};

isConnectionEstablished();


module.exports = {
    dbConnection,
    doQuery
};

